Feature: Authenticated Users can be notified of events that require their attention

Purpose: Users are able to respond to project events in a simple, transparent, and timely way

![Notifications](asset://assets/Mobile%20Screenshots/05%20-%20All%20Roles%20-%20Using%20Artisan/05.01.02_Menu.png)

Scenario Outline: The Authenticated User receives a notification of an event

	Given the Authenticated User is logged on to the Artisan system
	When a new "<Notifiable Event"> occurs
	Then they are alerted to that new notification
	And they can view it in a list of all their un-actioned notifications

Examples:
	|Notifiable Event|Notification|Development Status|
	|User assigned to "Primary Applicant Role"| Complete Project Set-up|Ready for Testing|
	|User assigned to "Building Primary Principle" Role| Complete Project Set-up|Ready for Testing|
	|Last requirement met in Shotlist|Shotlist Ready for Submission|Not Started|
	|Shotlist Failed|Shotlist Failed|Not Started|
	|Shotlist Passed|Shotlist Passed|Not Started|



Scenario: The Authenticated User ignores the event Notification but completes the action anyway
	
	Given the Authenticated User has received a Notifiable Event notification
	But has ignored it
	And has completed the action expected anyway
	Then the notification is removed on completion of that expected action