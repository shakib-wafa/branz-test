Feature: Authenticated Users can use complex criteria to search and filter the Project List for relevant Projects of interest

Purpose: Users are helped to locate specific Projects they're interested in when the list of projects is large - this may be particularly useful to Consenters who may deal with many active projects at once.

Scenario: The Authenticated User finds a small number of projects by supplying several discrete criteria

	Given the Authenticated User is viewing an unfiltered Projects List
	And they have selected the Advanced Search option
	And they have entered a Project creation date start range of after "01-01-2018"
	And they have entered a Project creation date end range of before "01-02-2018"
	And they have entered a Building Participant with the first name of "James"
	And they have entered an Address Street Name of "Ranfurly Place"
	And they have opted to perform the search
	Then they will see only Projects matching the given criteria in their Projects List


Scenario: The Authenticated User clears the search criteria

	Given the Authenticated User is viewing a Projects List filtered by an Advanced Search
	When they select the option to clear the search
	Then their Search Criteria is reset
	And the Projects List shows an unfiltered list


Scenario: The Authenticated User's Advanced Search returns no results

	Given the Authenticated User's specified advanced search criteria returned no results
	Then the user is shown an empty Projects List
	And a notification confirming that no results were found