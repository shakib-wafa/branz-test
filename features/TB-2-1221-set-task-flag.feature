Feature: Authenticated Users can search and filter the Project List for relevant Projects of interest

Purpose: Users are helped to quickly locate the Projects they're interested in

![Search](asset://assets/Mobile%20Screenshots/05%20-%20All%20Roles%20-%20Using%20Artisan/05.02.01_Project_List.png)

@ready_to_test
Scenario: The Authenticated User finds a project by entering part of the associated address
_(simple developed scenario that needs describing as part of test preparation in next phase)_

@ready_to_test
Scenario: The Authenticated User's search returns no results
_(simple developed scenario that needs describing as part of test preparation in next phase)_

@ready_to_test
Scenario: The Authenticated User clears the search criteria
_(simple developed scenario that needs describing as part of test preparation in next phase)_

@ready_to_test
Scenario: The Authenticated User toggles the Archived Projects filter to include Archived Projects in the Project List
_(simple developed scenario that needs describing as part of test preparation in next phase)_

@ready_to_test
Scenario: The Authenticated User toggles the Archived Projects filter to hide Archived Projects from the Project List
_(simple developed scenario that needs describing as part of test preparation in next phase)_