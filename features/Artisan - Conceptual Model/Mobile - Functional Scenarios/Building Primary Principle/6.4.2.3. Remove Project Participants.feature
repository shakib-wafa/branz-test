Feature: Building Primary Participants can remove participants from a project

Purpose: Access to a project can be managed. 


Scenario: The Building Primary Participant removes the user from the project 

	Given The Building Primary Principles has selected a project
	And they have selected the option to view the list of project participants
	When they select the user 
	And select the "remove" option 
	Then the user is removed as a participant from the project 
	And the system sends a "removal" notification to the participant


Scenario: The Building Primary restores the removed user from the project 

	Given The Building Primary Principle has removed a participant from the project 
	Then they must re-invite the user back in to the Project 