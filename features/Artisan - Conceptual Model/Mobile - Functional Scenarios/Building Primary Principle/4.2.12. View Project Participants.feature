Feature: Building Primary Principles can see a list of all Project Participants and their roles

Purpose: The Building Primary Principles can see who is working on the project, who has yet to accept an invite, and generally manage the list of participants on their project.


![Project Participants](asset://assets/Mobile%20Screenshots/03%20-%20BPP%20-%20Complete%20Project%20Set-up/03.02.12_Project-settings_Participants.png)

_Note: As the above screenshot shows, this has not been implemented on the Android platform._

Scenario: The Building Primary Principles views the list of participants for the project
	Given The Building Primary Principles has selected a project
	When they select the option to view the list of project participants
	Then they see a list of project participants