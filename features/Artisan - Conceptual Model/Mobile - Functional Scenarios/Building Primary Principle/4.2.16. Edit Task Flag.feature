Feature: The Building Primary Principles can select a Flag from a Shotlist Task and make it not-applicable

Purpose: Building Participants do not see flags that may not be relevant to their situation.


Scenario: The Building Primary Principles deselects a Flag

	Given The Building Primary Principles is reviewing Shotlist Tasks whilst completing a project set-up
	And a particular Task has a Flag on it
	When they select the Flag they can see the details
	And decide to mark the Flag as not-applicable
	And they can see the Flag remain but change to a not-applicable state
	Then the Flag will not show in the Project when it is running


Scenario: The Council convinces the Building Primary Principles that the Flag is essential and needs restoring after the "Complete Project Set-up" process has been finished

_This is not currently handled as there doesn't appear to be a way for the BPP to return to "Complete Project Set-up" mode_