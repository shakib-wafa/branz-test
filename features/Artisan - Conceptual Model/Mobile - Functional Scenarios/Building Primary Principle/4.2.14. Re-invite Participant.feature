Feature: The Building Primary Principle can re-invite Participants who have not yet accepted

Purpose: Participants can be helped if their invite email gets lost in their inbox and the Building Primary Principle or Primary Applicant can proactively nudge the stakeholders to address the project.

![Re-invite Participant](asset://assets/Mobile%20Screenshots/03%20-%20BPP%20-%20Complete%20Project%20Set-up/03.02.13a_Participants_sub-menu.png)

Scenario: The Building Primary Principle reviews the Project's Participant list and re-invites non-responders
_It's not clear how the BPP can re-invite participants once the Project Set-up have been completed - or even what the purpose of 'Invite status' is, unless it's expected that it may take hours or days to complete the project set-up process (allowing time for invitees to respond)_

	Given The Building Primary Principle is reviewing the Participant List associated with a Project whilst completing a project set-up
	And a participant has an Invite status of "Sent"
	When they select the option to re-invite the participant
	Then the selected participant will receive a new invite notification
	And the Invite status timestamp will be set to the current time