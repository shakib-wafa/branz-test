Feature: Project Participants can annotate a photo they have attached to a Task Requirement using simple drawing tools

Purpose: The participant can highlight important areas of the photo to expedite the review


Scenario: The Participant circles an important feature of the photo

	Given the Participant has selected a Photo attached to the Task Requirement
	And they have selected a drawing tool
	And they may have selected a line colour
	When they draw a circle path with their finger around an area in the photo
	Then a circular line appears following their path on the drawing layer on top of the photo


Scenario: The Participant corrects a drawing mistake

	Given the Participant has just drawn a shape
	When they opt to "undo" their drawing
	Then the shape they just drew is removed from the drawing layer on top of the photo


Scenario: The Participant recovers a removed correction to a drawing

	Given the Participant has just opted to "undo" a drawing but wants it back
	When they opt to "redo" their drawing
	Then the shape they just removed is redrawn


Scenario: The Participant closes the photo and either navigates away or chooses another photo

	Given the Participant navigates away from the photo
	When they re-select the photo
	Then the "undo" and "redo" options are disabled until a new drawing is added to the drawing layer


Scenario: The Participant toggles the drawing layer on top of the photo

	Given the Participant has selected a Photo attached to the Task Requirement
	When they toggle the "show|hide" option
	Then the drawing layer on top of the photo will be "visible|invisible"
	And the drawing tools will be "active|inactive"