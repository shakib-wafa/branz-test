Feature: Project Participants receive an invitation to join a project

Purpose: Participants can quickly and simply access a new project

![Pending Invites](asset://assets/Mobile%20Screenshots%2F05%20-%20All%20Roles%20-%20Using%20Artisan/05.04.01_Pending_Invites-not%20actual%20screenshot.png)

_Question: This seems like a duplication of the "Notifications" feature - albeit, focused on the single use case of being notified about a project. The corresponding "Pending Invites" link in the Android menu does not seem to work for any user and may not have been implemented. So this functionality might be deprecated in favour of the more general "Notifications feature"?_


Scenario: The Project Participant views an invitation to a new Project

	Given the Participant has been invited to a project
	And they have not yet accepted the invitation
	When they are alerted to a new invitation notification
	And they view the list of pending invites
	And they select an invitation
	Then they are taken to the respective project's participant registration form
	And the notification message is removed from their list of pending invites