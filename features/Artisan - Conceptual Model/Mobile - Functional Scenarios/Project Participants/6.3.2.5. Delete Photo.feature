Feature: Project Participants can delete the photo from the attachment

Purpose: The participant can remove a photo uploaded in error


Scenario: The Participant deletes the image from the Task Requirement

	Given the Participant has selected a Photo attached to the Task Requirement
	When they select the option to "delete"
	Then the selected photo is removed
	And the system retains the photo for audit and recovery purposes


Scenario: the Participant wishes to revert to the old photo
_Note: this is not handled. It could be implemented as an immediate "undo" operation, a "select the image from device again" feature, or "search and attach from system archive for this requirement"._