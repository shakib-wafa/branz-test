Feature: Project Participants can zoom, pan, and rotate a photo

Purpose: The participant can ensure the correct details are clear and unambiguous


Scenario: The Participant zooms into an area of the photo

	Given the Participant has selected a Photo attached to the Task Requirement
	When they select the option to "zoom"
	Then the photo is resized from the centre of the frame


Scenario: The Participant moves an enlarged photo to centre specific elements of interest

	Given the Participant has selected a photo which has been zoomed and has hidden content that overflows the frame
	When they drag on the photo
	Then the photo moves in the direction of their drag until there is no more hidden content


Scenario: The Participant rotates the photo

	Given the Participant has selected a Photo attached to the Task Requirement
	When the select the option to rotate the photo "Clockwise 90˚|Anti-clockwise 90˚"
	Then the photo is rotated "+90˚|-90˚"