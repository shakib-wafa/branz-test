Feature: Project Participants can updated a selected photo with a new photo

Purpose: The participant can correct a sub-standard photo
_Question: is this only before submission, or is this intended for use to address failed reviews?_


Scenario: the Participant updates a selected photo

	Given the Participant has selected a Photo attached to the Task Requirement
	When they select the option to "update"
	Then they take another photo
	And the new photo replaces the old photo
	And the system archives the previous photo and drawing layer


Scenario: the Participant wishes to revert to the old photo
_Note: this is not handled. It could be implemented as an immediate "undo" operation, a "select another image from device" feature, or "search and attach from system archive for this requirement"._

	