At the conceptual level, Artisan is made from system components. These components satisfy one or more functional requirements.

The following model has been created to provide a solid foundation for product management - and to provide a coherent and comprehensive map that defines the solution implementation. The model is divided into "Abilities" (or functional areas) and "Business Needs" (or non-functional areas).

The solution's Abilities - or, the tangible features needed to support the user goals - have been partitioned into eight sub-domains. These help bring order and differentiate various concerns.

The solution's Business Needs - or, the environmental and design factors that ensure user goals are met - have been partitioned into four sub-domains.

Each sub-domain contains a list of related components against which functional scenario's "inject" their requirements (scenario steps). These components must be (and in most cases, already have been) implemented across the appropriate channels (Web, Mobile, or both).

As the conceptual component model outlines a system implementation perspective - it provides an important cross-reference against the user perspective of the functional requirements model.

![Artisan Concept Model](asset://assets/Artisan-Features-small.png)
View the model here: [Artisan Concept Model](/certus/BRANZ/eokQDWdILw8A/assets/Artisan-Features.png)
