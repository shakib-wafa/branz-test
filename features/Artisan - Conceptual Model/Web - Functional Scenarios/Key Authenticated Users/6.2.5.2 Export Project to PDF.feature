Feature: Key Authenticated Users can export the project as a PDF document containing all Shotlist and attachment data

Purpose: Data that Artisan has enabled user to gather can be shared externally (outside the Artisan system) to support the user's own quality and process improvement audits and training, and inform stakedholders.

Scenario: The Key Authenticated User exports the project as a PDF file

	Given the Key Authenticated User has selected a project they wish to extract artifacts from
	When they select the option to export Project to PDF
	Then they are prompted to save a PDF file containing all the project's Status Summary, Shotlists, Tasks, Requirements, and Attachments as a single document