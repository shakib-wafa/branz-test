Feature: Key Authenticated Users can export project images and notes

Purpose: Data that Artisan has enabled user to gather can be used externally (outside the Artisan system) to support the user's own quality and process improvement audits and training

Scenario: The Key Authenticated User exports all project artifacts gathered to-date

	Given the Key Authenticated User has selected a project they wish to extract artifacts from
	When they select the option to export artifacts
	Then they are prompted to save a compressed archive (a zip file) containing all the projects images and note files stored in a structure of folders that mirrors the Shotlist, Task, Requirement, and Attachment hierarchy