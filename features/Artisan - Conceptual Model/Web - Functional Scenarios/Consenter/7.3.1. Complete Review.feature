Feature: A Consenter has completed reviewing a submitted Shotlist's attachments and wishes to publish their assessment

Purpose: To allow the Consenter to complete and review their assessment of all attachments before providing feedback to the Project team.

![Complete Review](asset://assets/Web%20Screenshots/07%20-%20Consenter%20-%20Reviewing%20and%20Consenting/07.03%20-%20Submit%20Review/07.03.01a_Project_Shotlist_Submit_Review.png)

Scenario: The Consenter has completed their review of the Shotlist and submits their review containing Failed attachment reviews

	Given the Consenter has completed a review of all Shotlist attachments
	And has found at least one attachment to have failed
	And has submitted their review
	Then the associated Requirement, Task, and Shotlist status is set to "Failed"
	And the ability to submit other Shotlists within the Project is disabled


Scenario: The Consenter has completed their review of the Shotlist and submits their review containing only passed attachment reviews

	Given the Consenter has completed a review of all Shotlist attachments
	And has passed all attachments
	And has submitted their review
	Then the associated Requirement, Task, and Shotlist status is set to "Review Complete"
	And the ability to submit other Shotlists within the Project is enabled


Scenario: The Consenter has not completed their review of the Shotlist, with some attachments still needing review

	Given the Consenter has not completed a review of all Shotlist attachments
	And has not set at least one attachment to "Passed|Failed"
	Then the ability to Complete Review is disabled