Feature: Consenters can quickly filter the Project List for only Project that are In Review

Purpose: To help Consenters quickly locate the Projects they're ready to work on

![Filter for In-Review Projects](asset://assets/Web%20Screenshots/05%20-%20All%20Roles%20-%20Using%20Artisan/05.02%20-%20Projects%20List/05.02.05.01_Projects_List_In-Review_Filter.png)

Scenario: The Consenter wants to see a list of Projects that are In Review

	Given The Consenter is viewing the Projects List
	When they see there is 1 or more projects ready for review on the In Review counter
	Then they toggle a filter
	And they see only Projects in the state of In Review in their Projects List


Scenario: There are no Projects currently in review

	Given The there are no Project currently in the In Review status
	Then the In Review toggle is disabled
	And the In Review counter is set to "0"


Scenario: The Consenter wants to see all Projects

	Given The Consenter has toggled on the In Review filter
	When the user toggles-off the In Review filter
	Then the Projects List display all Projects