Feature: A Consenter may fail an attachment that does not pass inspection

Purpose: Essential building standards are enforced and reported and the Building Participant receives a clear understanding of the issue that needs addressing.

![Fail an Attachment](asset://assets/Web%20Screenshots/07%20-%20Consenter%20-%20Reviewing%20and%20Consenting/07.01%20-%20Review%20Requirements%20Attachment/07.01.03a_Requirement_Review_Fail.png)

Scenario: The Consenter fails a particular attachment
_Note: the Review Reason functionality has not yet been implemented_
	Given The Consenter is in the process of reviewing a Shotlist
	When they find an issue or problem with an attachment relating to a Task Requirement
	Then they fail the selected attachment
	And they are prompted to enter a mandatory Review Reason note
	And they enter at least "50" characters outlining their reason
	And the system accepts their Failed assessment of the Attachment
