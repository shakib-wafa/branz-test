Feature: Shotlist Administrators can delete unused Categories in the Master Shotlist Template

Purpose: They can maintain a well organised master template.


Scenario: Deleting an unused Category

	Given the shotlist administrator has created a template category
	When the template category has no associated template tasks
	Then the shotlist administrator can delete the template category


Scenario: Deleting a used Category

	Given the shotlist administrator has created a template category
	When the template category has one or more associated template tasks
	Then they cannot delete the template category
	And they are informed why