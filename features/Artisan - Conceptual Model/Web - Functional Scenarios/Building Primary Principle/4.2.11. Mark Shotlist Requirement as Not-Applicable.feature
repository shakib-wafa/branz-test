Feature: The Building Primary Principle can select a Requirement for a Shotlist Task and make it not-applicable

Purpose: Building Participants do not see requirements that may not be relevant to their situation.

![Review Task Requirements](asset://assets/Web%20Screenshots/03%20-%20BPP%20-%20Complete%20Project%20Setup/04.02.10_Review_Task_Requirements.png)

Scenario: The Building Primary Principle deselects a Task Requirement

	Given The Building Primary Principle is reviewing Task Requirements whilst completing a project set-up
	And they choose a Requirement to deselect
	When they mark that Requirement as not-applicable
	Then they can see the Requirement remain but change to a not-applicable state
	And the Requirement will not show in the Project when it is running


Scenario: The Building Primary Principle determines that the Requirement is actually applicable after the "Complete Project Set-up" process has been finished

_This is not currently handled as there doesn't appear to be a way for the BPP to return to "Complete Project Set-up" mode_