Feature: Building Primary Principles can quickly filter the Project List for Projects with failed Shotlists

Purpose: To help Building Primary Principles quickly locate the Project Shotlists that need remediation

![Tasks with Flags](asset://assets/Web%20Screenshots/03%20-%20BPP%20-%20Complete%20Project%20Setup/05.02.05.01_Project_List_Failed_Filter.png)

Scenario: The Building Primary Principle wants to see a list of Projects that have failed Shotlists

	Given The Building Primary Principle is viewing the Projects List
	When they see there is 1 or more projects with failed Shotlists in the Failed counter
	Then they toggle a filter
	And they see only Projects with Shotlists in a Failed state in their Projects List


Scenario: There are no Projects currently with failed Shotlists

	Given The there are no Project currently with Shotlists in a Failed state
	Then the Failed toggle is disabled
	And the Failed counter is set to "0"


Scenario: The Building Primary Principle wants to see all Projects

	Given The Building Primary Principle has toggled on the Failed filter
	When the user toggles-off the Failed filter
	Then the Projects List display all Projects
