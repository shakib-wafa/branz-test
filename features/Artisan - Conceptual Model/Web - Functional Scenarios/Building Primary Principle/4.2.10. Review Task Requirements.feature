Feature: The Building Primary Principle can review the Requirements of a Task in a Shotlist

Purpose: Building Primary Principle can ensure the Requirements are relevant to their situation.

![Review Task Requirements](asset://assets/Web%20Screenshots/03%20-%20BPP%20-%20Complete%20Project%20Setup/04.02.10_Review_Task_Requirements.png)

Scenario: The Building Primary Principle deselects a Task Requirement

	Given The Building Primary Principle is reviewing a Shotlist Task whilst completing a project set-up
	And they select a particular Task
	Then they can see the list of associated Requirements with the ability to Mark Shotlist Requirement as Not-Applicable
