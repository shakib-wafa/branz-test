Feature: Authenticated Users can navigate directly to places in the Artisan system that an Notification relates to

Purpose: Users are able to expedite their work and enjoy a frictionless user experience.

Scenario Outline: The Authenticated User responds to a notification
_Note: it might be a less than optimal experience to receive an actionable notification on the Web Console, such as "Last requirement met in Shotlist" - for when a Shotlist is ready to submit, then be told to change devices to action. If some actionable events are not shown depending on channel (Web or Mobile), that may lead to an inconsistent experience. The simplest solution is to mirror such functionality on all (both) channels._

	Given the Authenticated User is viewing a "<Notifiable Event>" in their Notifications List
	When they select a specific "<Notifiable Event"> notice
	Then they are taken to the correct "<Corresponding Place>" in the system
	And the notification is removed from their Notifications List

Examples:
	|Notifiable Event|Corresponding Place|Development Status|
	|User assigned to "Primary Applicant Role"| Review Project Set-up|Ready for Testing|
	|User assigned to "Building Primary Principle" Role| Review Project Set-up|Ready for Testing|
	|Last requirement met in Shotlist|Log into Mobile App to Submit|Not Started|
	|Shotlist Failed|Shotlist Failed|First Failed Requirement|
	|Shotlist Passed|Shotlist Passed|Next Shotlist|
