Feature: Authenticated Users can see a list of activities across all the projects they are associated with

Purpose: Collaboration and communication is encouraged, transparency is provided, and preparation for dependent tasks is supported.

Scenario Outline: The Authenticated User wishes to gain an understanding of recent activities affecting projects they are a stakeholder in

	Given the Authenticated User is logged on the Artisan system
	When they select the option to view Activites
	Then they can see a simple list of activities containing entries such as "<activity item>" arranged in order of latest to oldest
	And they can opt to navigate to the source of the activity item

Examples:
	|activity item|
	|New Project has been created|
	|A participant has been added to the project in a role|
	|An attachment has been added|
	|A requirement has been met|
	|A task has been completed|
	|A shotlist has been completed|
	|A shotlist has been submitted for review|
	|A shotlist has had its review completed|
	|A shotlist has failed|
	|A attachment has failed|