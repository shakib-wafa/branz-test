Feature: Unregistered Users can upload an image to represent their user in the Artisan system

Purpose: Users can personalise their presence within the system and provide visual cues when collaborating with other users within the system.

![Upload Profile Image](asset://assets/Web%20Screenshots/04%20-%20All%20Roles%20-%20Register%20for%20Artisan/04.02%20-%20Register%20Profile/04.02.01.01_Registration.png)

Scenario: The unregistered user uploads a profile image

	Given an unregistered user is in the process of registering to use Artisan
	When they chose to upload a profile image
	And they select an image file of type [.jpg|.jpeg|.png] from their local computer
	Then they can submit this image with their registration profile

