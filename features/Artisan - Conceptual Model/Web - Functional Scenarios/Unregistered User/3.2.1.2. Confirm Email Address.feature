Feature: Unregistered Users can confirm their email address when they register to use Artisan

Purpose: Users can receive reliably email notifications.

Scenario: The unregistered user enters a correct email and confirms their email address

	Given an unregistered user has completed the registration form and has submitted the form
	Then they will be asked to check their given email account for a confirmation notice
	And the system emails their given email address with an email containing a confirmation link
	When they select the confirmation link in their email they are taken to the Artisan Website and can see a confirmation message
	Then they acknowledge the confirmation message and are taken to the login screen


Scenario: The unregistered user enters an incorrect email address
	Given an unregistered user has completed the registration form and has submitted the form
	Then they will be asked to check their given email account for a confirmation notice
	When they notice their given email address is incorrect
	And they can return from the confirmation request notice to the registration form
	Then enter their correct email address
	

Scenario: The unregistered user does not receive an email confirmation email
	Given an unregistered user has completed the registration form and has submitted the form
	Then they will be asked to check their given email account for a confirmation notice
	When a confirmation email does not arrive
	And they can return from the confirmation request notice to the registration form
	Then verify their email address is correct and proceed again


Scenario: The unregistered user enters an email address that is already in the system
	Given an unregistered user has completed the registration form and has submitted the form
	When the given email address is already in the system
	And they will be asked to either login or reset their password
	And they can return from the exception message
	Then verify their email address is correct and proceed again